using System;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MeshGenerator : MonoBehaviour
{
    public SquareGrid squareGrid;
    public MeshFilter walls;
    public MeshFilter cave;

    public bool is2D;
    private List<Vector3> _vertices;
    private List<int> _trianglesFromVertexIndices;

    private Dictionary<int, List<Triangle>> verticesSharedByTriangles = new Dictionary<int, List<Triangle>>();
    private readonly List<List<int>> _outlines = new List<List<int>>(); // _outlines cannot be re-assigned
    private HashSet<int> _checkedVertices = new HashSet<int>();
    
    public void GenerateMesh(int[,] map, float squareSize)
    {
        verticesSharedByTriangles.Clear();
        _outlines.Clear();
        _checkedVertices.Clear();
        squareGrid = new SquareGrid(map, squareSize);
        _vertices = new List<Vector3>();
        _trianglesFromVertexIndices = new List<int>();

        for (int x = 0; x < squareGrid.squares.GetLength(0); ++x)
        {
            for (int y = 0; y < squareGrid.squares.GetLength(1); ++y)
            {
                TriangulateSquare(squareGrid.squares[x, y]);
            }
        }

        Mesh mesh = new Mesh();
        cave.mesh = mesh;

        mesh.vertices = _vertices.ToArray();
        mesh.triangles = _trianglesFromVertexIndices.ToArray();
        mesh.RecalculateNormals();

        int tileAmount = 4;
        Vector2[] texCoords = new Vector2[_vertices.Count];
        for(int i = 0; i < texCoords.Length; ++i)
        {
            float percentX = tileAmount * Mathf.InverseLerp(-map.GetLength(0) / 2 * squareSize, map.GetLength(0) / 2 * squareSize, _vertices[i].x);
            float percentZ = tileAmount * Mathf.InverseLerp(-map.GetLength(0) / 2 * squareSize, map.GetLength(0) / 2 * squareSize, _vertices[i].z);
            texCoords[i] = new Vector2(percentX, percentZ);
        }
        mesh.uv = texCoords;

        if (is2D)
        {
            Generate2DColliders();
        }
        else
        {
            CreateWallMesh();
        }
    }

    private void Generate2DColliders()
    {
        EdgeCollider2D[] currentColliders = gameObject.GetComponents<EdgeCollider2D>();
        for(int i = 0; i < currentColliders.Length; ++i)
        {
            Destroy(currentColliders[i]);
        }

        CalculateMeshOutlines();
        
        foreach(List<int> outline in _outlines) {
            EdgeCollider2D edgeCollider = gameObject.AddComponent<EdgeCollider2D>();
            Vector2[] edgePoints = new Vector2[outline.Count];

            for (int i = 0; i < outline.Count; ++i)
            {
                Vector3 outlineVertex = _vertices[outline[i]];
                edgePoints[i] = new Vector2(outlineVertex.x, outlineVertex.z);
            }
            edgeCollider.points= edgePoints;
        }
    }

    private void CreateWallMesh()
    {
        CalculateMeshOutlines();
        
        List<Vector3> wallVertices = new List<Vector3>();
        List<int> wallTriangles = new List<int>();
        Mesh wallMesh = new Mesh();
        float wallHeight = 5;

        foreach (List<int> outline in _outlines)
        {
            for (int i = 0; i < outline.Count - 1; ++i)
            {
                // create vertices
                int startIndex = wallVertices.Count;
                wallVertices.Add(_vertices[outline[i]]); // left
                wallVertices.Add(_vertices[outline[i+1]]); // right
                wallVertices.Add(_vertices[outline[i]] - Vector3.up * wallHeight); // bottom left
                wallVertices.Add(_vertices[outline[i+1]] - Vector3.up * wallHeight); // bottom right
                
                // create triangles using vertex indices
                wallTriangles.Add(startIndex + 0);
                wallTriangles.Add(startIndex + 2);
                wallTriangles.Add(startIndex + 3);
                
                wallTriangles.Add(startIndex + 3);
                wallTriangles.Add(startIndex + 1);
                wallTriangles.Add(startIndex + 0);
            }
        }

        wallMesh.vertices = wallVertices.ToArray();
        wallMesh.triangles = wallTriangles.ToArray();
        walls.mesh = wallMesh;

        MeshCollider wallCollider = walls.gameObject.AddComponent<MeshCollider>();
        wallCollider.sharedMesh= wallMesh;
    }

    private void TriangulateSquare(Square square)
    {
        switch (square.configuration)
        {
            case 0: break;

            // 1 points:
            case 1:
                MeshFromPoints(square.centerLeft, square.centerBottom, square.bottomLeft);
                break;
            case 2:
                MeshFromPoints(square.bottomRight, square.centerBottom, square.centerRight);
                break;
            case 4:
                MeshFromPoints(square.topRight, square.centerRight, square.centerTop);
                break;
            case 8:
                MeshFromPoints(square.topLeft, square.centerTop, square.centerLeft);
                break;

            // 2 points:
            case 3:
                MeshFromPoints(square.centerRight, square.bottomRight, square.bottomLeft, square.centerLeft);
                break;
            case 6:
                MeshFromPoints(square.centerTop, square.topRight, square.bottomRight, square.centerBottom);
                break;
            case 9:
                MeshFromPoints(square.topLeft, square.centerTop, square.centerBottom, square.bottomLeft);
                break;
            case 12:
                MeshFromPoints(square.topLeft, square.topRight, square.centerRight, square.centerLeft);
                break;
            case 5:
                MeshFromPoints(square.centerTop, square.topRight, square.centerRight, square.centerBottom,
                    square.bottomLeft, square.centerLeft);
                break;
            case 10:
                MeshFromPoints(square.topLeft, square.centerTop, square.centerRight, square.bottomRight,
                    square.centerBottom, square.centerLeft);
                break;

            // 3 points:
            case 7:
                MeshFromPoints(square.centerTop, square.topRight, square.bottomRight, square.bottomLeft,
                    square.centerLeft);
                break;
            case 11:
                MeshFromPoints(square.topLeft, square.centerTop, square.centerRight, square.bottomRight,
                    square.bottomLeft);
                break;
            case 13:
                MeshFromPoints(square.topLeft, square.topRight, square.centerRight, square.centerBottom,
                    square.bottomLeft);
                break;
            case 14:
                MeshFromPoints(square.topLeft, square.topRight, square.bottomRight, square.centerBottom,
                    square.centerLeft);
                break;

            // 4 points:
            case 15:
                // können kein outline bilden, also kann man sie direkt als checked eintragen.
                MeshFromPoints(square.topLeft, square.topRight, square.bottomRight, square.bottomLeft);
                _checkedVertices.Add(square.topLeft.vertexIndex);
                _checkedVertices.Add(square.topRight.vertexIndex);
                _checkedVertices.Add(square.bottomRight.vertexIndex);
                _checkedVertices.Add(square.bottomLeft.vertexIndex);
                break;
        }
    }

    private void MeshFromPoints(params Node[] points)
    {
        AssignVertices(points);

        if (points.Length >= 3)
            CreateTriangle(points[0], points[1], points[2]);
        if (points.Length >= 4)
            CreateTriangle(points[0], points[2], points[3]);
        if (points.Length >= 5)
            CreateTriangle(points[0], points[3], points[4]);
        if (points.Length >= 6)
            CreateTriangle(points[0], points[4], points[5]);
    }

    private void AssignVertices(Node[] points)
    {
        for (int i = 0; i < points.Length; ++i)
        {
            if (points[i].vertexIndex == -1)
            {
                points[i].vertexIndex = _vertices.Count;
                _vertices.Add(points[i].position);
            }
        }
    }

    private void CreateTriangle(Node a, Node b, Node c)
    {
        _trianglesFromVertexIndices.Add(a.vertexIndex);
        _trianglesFromVertexIndices.Add(b.vertexIndex);
        _trianglesFromVertexIndices.Add(c.vertexIndex);

        Triangle triangle = new Triangle(a.vertexIndex, b.vertexIndex, c.vertexIndex);
        AddTriangleToDictionary(triangle.vertexIndexA, triangle);
        AddTriangleToDictionary(triangle.vertexIndexB, triangle);
        AddTriangleToDictionary(triangle.vertexIndexC, triangle);
    }

    private void AddTriangleToDictionary(int vertexIndexKey, Triangle triangle)
    {
        if (verticesSharedByTriangles.ContainsKey(vertexIndexKey))
        {
            verticesSharedByTriangles[vertexIndexKey].Add(triangle);
        }
        else
        {
            List<Triangle> triangleList = new List<Triangle> {triangle};
            verticesSharedByTriangles.Add(vertexIndexKey, triangleList);
        }
    }

    private void CalculateMeshOutlines()
    {
        // if an outline vertex is found, then one iteration calculates the complete outline in FollowOutline
        for (int vertexIndex = 0; vertexIndex < _vertices.Count; ++vertexIndex)
        {
            if (!_checkedVertices.Contains(vertexIndex))
            {
                int newOutlineVertex = GetConnectedOutlineVertex(vertexIndex);
                if (newOutlineVertex != -1)
                {
                    _checkedVertices.Add(vertexIndex);

                    List<int> newOutline = new List<int> { vertexIndex };
                    _outlines.Add(newOutline);
                    FollowOutline(newOutlineVertex, _outlines.Count - 1);
                    // at the end append the starting vertex so the outline ends where it started
                    _outlines[_outlines.Count - 1].Add(vertexIndex);
                }
            }
        }
    }

    // Creates the complete outline at outlineIndex for a given vertex that is part of an outline edge.
    private void FollowOutline(int vertexIndex, int outlineIndex)
    {
        _outlines[outlineIndex].Add(vertexIndex);
        _checkedVertices.Add(vertexIndex);
        int nextVertexIndex = GetConnectedOutlineVertex(vertexIndex);

        if (nextVertexIndex != -1)
        {
            FollowOutline(nextVertexIndex, outlineIndex);
        }
    }

    private int GetConnectedOutlineVertex(int vertexIndex)
    {
        List<Triangle> trianglesContainingVertex = verticesSharedByTriangles[vertexIndex];

        foreach (Triangle triangle in trianglesContainingVertex)
        {
            for (int i = 0; i < 3; ++i)
            {
                int vertexB = triangle[i];
                if (vertexB != vertexIndex && !_checkedVertices.Contains(vertexB))
                {
                    if (IsOutLineEdge(vertexIndex, vertexB))
                    {
                        return vertexB;
                    }
                }
            }
        }

        return -1;
    }

    // wenn A und B zusammen in mehr oder weniger als 1 triangle enthalten sind
    // dann bilden sie zusammen keine outline edge
    // geht nur für meshes die abgeschnitten sind / keine backfaces haben / nicht komplett ummantelt 
    private bool IsOutLineEdge(int vertexA, int vertexB)
    {
        List<Triangle> trianglesContainingVertexA = verticesSharedByTriangles[vertexA];
        int sharedTriangleCount = 0;

        for (int i = 0; i < trianglesContainingVertexA.Count; ++i)
        {
            if (trianglesContainingVertexA[i].Contains(vertexB))
            {
                sharedTriangleCount++;
                if (sharedTriangleCount > 1)
                {
                    return false;
                }
            }
        }

        return sharedTriangleCount == 1;
    }

    struct Triangle
    {
        public int vertexIndexA;
        public int vertexIndexB;
        public int vertexIndexC;
        private int[] vertices;

        public Triangle(int a, int b, int c)
        {
            vertexIndexA = a;
            vertexIndexB = b;
            vertexIndexC = c;

            vertices = new int[3];
            vertices[0] = a;
            vertices[1] = b;
            vertices[2] = c;
        }

        public int this[int i]
        {
            get { return vertices[i]; }
        }

        public bool Contains(int vertexIndex)
        {
            return vertexIndex == vertexIndexA || vertexIndex == vertexIndexB || vertexIndex == vertexIndexC;
        }
    }

    private void OnDrawGizmos()
    {
        if (squareGrid != null)
        {
            // draw based on sqaures
            for (int x = 0; x < squareGrid.squares.GetLength(0); ++x)
            {
                for (int y = 0; y < squareGrid.squares.GetLength(1); ++y)
                {
                    Gizmos.color = squareGrid.squares[x, y].topLeft.active ? Color.black : Color.white;
                    Gizmos.DrawCube(squareGrid.squares[x, y].topLeft.position, Vector3.one * 0.4f);

                    Gizmos.color = squareGrid.squares[x, y].topRight.active ? Color.black : Color.white;
                    Gizmos.DrawCube(squareGrid.squares[x, y].topRight.position, Vector3.one * 0.4f);

                    Gizmos.color = squareGrid.squares[x, y].bottomRight.active ? Color.black : Color.white;
                    Gizmos.DrawCube(squareGrid.squares[x, y].bottomRight.position, Vector3.one * 0.4f);

                    Gizmos.color = squareGrid.squares[x, y].bottomLeft.active ? Color.black : Color.white;
                    Gizmos.DrawCube(squareGrid.squares[x, y].bottomLeft.position, Vector3.one * 0.4f);

                    Gizmos.color = Color.grey;
                    Gizmos.DrawCube(squareGrid.squares[x, y].centerTop.position, Vector3.one * 0.15f);
                    Gizmos.DrawCube(squareGrid.squares[x, y].centerRight.position, Vector3.one * 0.15f);
                    Gizmos.DrawCube(squareGrid.squares[x, y].centerBottom.position, Vector3.one * 0.15f);
                    Gizmos.DrawCube(squareGrid.squares[x, y].centerLeft.position, Vector3.one * 0.15f);
                }
            }
        }
    }

    public class SquareGrid
    {
        public Square[,] squares;

        public SquareGrid(int[,] map, float squareSize)
        {
            int nodeCountX = map.GetLength(0); // width
            int nodeCountY = map.GetLength(1); // height
            float mapWidth = nodeCountX * squareSize;
            float mapHeight = nodeCountY * squareSize;

            ControlNode[,] controlNodes = new ControlNode[nodeCountX, nodeCountY];

            for (int x = 0; x < nodeCountX; ++x)
            {
                for (int y = 0; y < nodeCountY; ++y)
                {
                    float xPos = -mapWidth / 2 + x * squareSize + squareSize / 2;
                    float zPos = -mapHeight / 2 + y * squareSize + squareSize / 2;
                    Vector3 pos = new Vector3(xPos, 0, zPos);

                    bool active = map[x, y] == 1;
                    controlNodes[x, y] = new ControlNode(pos, active, squareSize);
                }
            }

            squares = new Square[nodeCountX - 1, nodeCountY - 1];
            for (int x = 0; x < nodeCountX - 1; ++x)
            {
                for (int y = 0; y < nodeCountY - 1; ++y)
                {
                    squares[x, y] = new Square(controlNodes[x, y + 1], controlNodes[x + 1, y + 1],
                        controlNodes[x + 1, y], controlNodes[x, y]);
                }
            }
        }
    }


    public class Square
    {
        public ControlNode topLeft, topRight, bottomRight, bottomLeft;
        public Node centerTop, centerRight, centerBottom, centerLeft;
        public int configuration;

        public Square(ControlNode _topLeft, ControlNode _topRight, ControlNode _bottomRight, ControlNode _bottomLeft)
        {
            topLeft = _topLeft;
            topRight = _topRight;
            bottomRight = _bottomRight;
            bottomLeft = _bottomLeft;

            centerTop = topLeft.right;
            centerRight = bottomRight.above;
            centerBottom = bottomLeft.right;
            centerLeft = bottomLeft.above;

            if (topLeft.active)
                configuration += 8;
            if (topRight.active)
                configuration += 4;
            if (bottomRight.active)
                configuration += 2;
            if (bottomLeft.active)
                configuration += 1;
        }
    }


    public class ControlNode : Node
    {
        public Node above, right;
        public bool active;

        public ControlNode(Vector3 _pos, bool _active, float squareSize) : base(_pos)
        {
            active = _active;
            above = new Node(position + Vector3.forward * squareSize / 2.0f);
            right = new Node(position + Vector3.right * squareSize / 2.0f);
        }
    }

    public class Node
    {
        public Vector3 position;
        public int vertexIndex = -1;

        public Node(Vector3 _pos)
        {
            position = _pos;
        }
    }
}
