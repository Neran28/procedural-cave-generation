using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;


// starting config ist eine map aus 0 und 1. Die muss dann noch gesmoothed werden. Die R�nder m�ssen immer schwarz sein.
public class MapGenerator : MonoBehaviour
{
    public int width;
    public int height;

    public string seed;
    public bool useRandomSeed;

    [Range(0, 100)] public int randomFillPercent;

    [Range(0, 20)] public int smoothIterations;

    [Range(0, 10)] public int borderSize;

    [Range(0, 100)] public int wallThresholdSize = 1;
    [Range(0, 100)] public int roomThresholdSize = 10;

    int[,] map;

    // falls man die map generation animieren will
    Coroutine c_mapGen;
    bool isRunning = false;
    
    private MeshGenerator _meshGen;

    public enum TileTypes
    {
        Room = 0,
        Wall = 1
    };
    
    
    private void Awake()
    {
        _meshGen = GetComponent<MeshGenerator>();
    }

    private void Start()
    {
        GenerateMap();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //if(isRunning)
            //    StopCoroutine(c_mapGen);
            GenerateMap();
        }
    }

    private void GenerateMap()
    {
        isRunning = true;

        // width ist #cols und height #rows
        // man k�nnte nun die loops �ndern auf for y < height for x < width map[x,y] aber das ist mega langsam, da kein sequentieller zugriff
        // man m�sste zus�tzlich den speicher so anordnen: int[height, width], dann hat man zeilenweise sequentiellen zugriff
        map = new int[width, height];
        RandomFillMap();

        for (int i = 0; i < smoothIterations; ++i)
        {
            SmoothMap();
        }
        
        ProcessMap();
        
        int[,] borderedMap = new int[width + borderSize * 2, height + borderSize * 2];
        
        for (int x = 0; x < borderedMap.GetLength(0); ++x)
        {
            for (int y = 0; y < borderedMap.GetLength(1); ++y)
            {
                // not in border area
                if (x >= borderSize && x < width + borderSize && y >= borderSize && y < height + borderSize)
                {
                    borderedMap[x, y] = map[x - borderSize, y - borderSize];
                }
                // border area
                else
                {
                    borderedMap[x, y] = (int)TileTypes.Wall;
                }
            }
        }

        _meshGen.GenerateMesh(borderedMap, 1);
        isRunning = false;
    }

    private void RandomFillMap()
    {
        if (useRandomSeed)
        {
            seed = Time.time.ToString();
        }

        System.Random prng = new System.Random(seed.GetHashCode());

        // spalten weise wird das grid aufgebaut
        for (int x = 0; x < width; ++x)
        {
            for (int y = 0; y < height; ++y)
            {
                if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                {
                    // the borders are always walls
                    map[x, y] = (int)TileTypes.Wall;
                }
                else
                {
                    // random depending on fill percent
                    map[x, y] = (prng.Next(0, 100) < randomFillPercent) ? (int)TileTypes.Wall : (int)TileTypes.Room;
                }
            }
        }
    }

    private void SmoothMap()
    {
        for (int x = 0; x < width; ++x)
        {
            for (int y = 0; y < height; ++y)
            {
                int neighbourWallCount = GetSurroundingWallCount(x, y);

                if (neighbourWallCount > 4)
                    map[x, y] = (int)TileTypes.Wall;
                else if (neighbourWallCount < 4)
                    map[x, y] = (int)TileTypes.Room;
            }
        }
    }

    private int GetSurroundingWallCount(int gridX, int gridY)
    {
        int wallCount = 0;
        for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; ++neighbourX)
        {
            for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; ++neighbourY)
            {
                if (IsInMapRange(neighbourX, neighbourY))
                {
                    if (neighbourX != gridX || neighbourY != gridY)
                    {
                        wallCount += map[neighbourX, neighbourY];
                    }
                }
                else
                {
                    wallCount++;
                }
            }
        }

        return wallCount;
    }

    struct Coord
    {
        public int tileX;
        public int tileY;

        public Coord(int x, int y)
        {
            tileX = x;
            tileY = y;
        }
    }

    List<Coord> GetRegionTiles(int startX, int startY)
    {
        List<Coord> tiles = new List<Coord>();
        int[,] mapFlags = new int[width, height];
        int tileType = map[startX, startY];

        Queue<Coord> queue = new Queue<Coord>();
        queue.Enqueue(new Coord(startX, startY));
        mapFlags[startX, startY] = 1;

        while (queue.Count > 0)
        {
            Coord tile = queue.Dequeue();
            tiles.Add(tile);

            for (int x = tile.tileX - 1; x <= tile.tileX + 1; ++x)
            {
                for (int y = tile.tileY - 1; y <= tile.tileY + 1; ++y)
                {
                    // && exclude diagonal neighbours
                    if (IsInMapRange(x, y) && (y == tile.tileY || x == tile.tileX))
                    {
                        if (mapFlags[x, y] != 0 || map[x, y] != tileType) continue;
                        
                        // add as visited tile
                        mapFlags[x, y] = 1;
                        queue.Enqueue(new Coord(x,y));
                    }
                }
            }
        }

        return tiles;
    }

    private void ProcessMap()
    {
        List<List<Coord>> wallRegions = GetRegions(1);

        foreach (List<Coord> wallRegion in wallRegions)
        {
            if (wallRegion.Count < wallThresholdSize)
            {
                foreach (Coord tile in wallRegion)
                {
                    map[tile.tileX, tile.tileY] = (int)TileTypes.Room;
                }
            }
        }
        
        List<List<Coord>> roomRegions = GetRegions(0);
        List<Room> survivingRooms = new List<Room>();

        foreach (List<Coord> roomRegion in roomRegions)
        {
            if (roomRegion.Count < roomThresholdSize)
            {
                foreach (Coord tile in roomRegion)
                {
                    map[tile.tileX, tile.tileY] = (int)TileTypes.Wall;
                }
            }
            else
            {
                survivingRooms.Add(new Room(roomRegion, map));
            }
        }
        survivingRooms.Sort();
        survivingRooms[0].isMainRoom= true;
        survivingRooms[0].isAccessibleFromMainRoom = true;
        ConnectClosestRooms(survivingRooms);
    }

    List<List<Coord>> GetRegions(int tileType)
    {
        List<List<Coord>> regions = new List<List<Coord>>();
        int[,] mapFlags = new int[width, height];

        for (int x = 0; x < width; ++x)
        {
            for (int y = 0; y < height; ++y)
            {
                if (mapFlags[x, y] == 0 && map[x, y] == tileType)
                {
                    List<Coord> newRegion = GetRegionTiles(x, y);
                    regions.Add(newRegion);

                    foreach (Coord tile in newRegion)
                    {
                        mapFlags[tile.tileX, tile.tileY] = 1;
                    }
                }
            }
        }

        return regions;
    }

    private bool IsInMapRange(int x, int y)
    {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    private void ConnectClosestRooms(List<Room> allRooms, bool forceAccessibilityFromMainRoom = false)
    {
        List<Room> roomListA = new List<Room>();
        List<Room> roomListB = new List<Room>();

        if(forceAccessibilityFromMainRoom)
        {
            foreach(Room room in allRooms)
            {
                if (room.isAccessibleFromMainRoom)
                { 
                    roomListB.Add(room);
                }
                else
                {
                    roomListA.Add(room);
                }
            }
        }
        else
        {
            roomListA = allRooms;
            roomListB = allRooms;
        }

        int shortestDistanceBetweenRoomAB = int.MaxValue;
        Coord bestTileA = new Coord();
        Coord bestTileB = new Coord();
        Room bestRoomA = null;
        Room bestRoomB = null;
        bool possibleConnectionFound = false;
        
        foreach (Room roomA in roomListA)
        {
            if (!forceAccessibilityFromMainRoom)
            {
                possibleConnectionFound = false;
            }

            foreach (Room roomB in roomListB)
            {
                if(roomA == roomB) continue;

                for (int tileIndexA = 0; tileIndexA < roomA.edgeTiles.Count; ++tileIndexA)
                {
                    for (int tileIndexB = 0; tileIndexB < roomB.edgeTiles.Count; ++tileIndexB)
                    {
                        Coord tileA = roomA.edgeTiles[tileIndexA];
                        Coord tileB = roomB.edgeTiles[tileIndexB];

                        int distanceBetweenTiles = (int)(Mathf.Pow(tileA.tileX - tileB.tileX, 2) + Mathf.Pow(tileA.tileY - tileB.tileY, 2));
                        if (distanceBetweenTiles < shortestDistanceBetweenRoomAB || !possibleConnectionFound)
                        {
                            shortestDistanceBetweenRoomAB = distanceBetweenTiles;
                            possibleConnectionFound = true;
                            bestTileA = tileA;
                            bestTileB = tileB;
                            bestRoomA = roomA;
                            bestRoomB = roomB;
                        }
                    }
                }
            }

            if (possibleConnectionFound && !bestRoomA.IsConnected(bestRoomB) && !forceAccessibilityFromMainRoom)
            {
                CreatePassageAndVisualize(bestRoomA, bestRoomB, bestTileA, bestTileB, Color.green);
            }
        }

        if(forceAccessibilityFromMainRoom && possibleConnectionFound)
        {
            CreatePassageAndVisualize(bestRoomA, bestRoomB, bestTileA, bestTileB, Color.magenta);
            ConnectClosestRooms(allRooms, true);
        }

        if (!forceAccessibilityFromMainRoom)
        {
            ConnectClosestRooms(allRooms, true);
        }
    }

    void CreatePassageAndVisualize(Room roomA, Room roomB, Coord tileA, Coord tileB, Color visColor)
    {
        Room.ConnectRooms(roomA, roomB);
        
        // visualize for now
        Debug.DrawLine(CoordToWorldPoint(tileA), CoordToWorldPoint(tileB), visColor, 20);

        List<Coord> line = GetLine(tileA, tileB);
        foreach(Coord coord in line)
        {
            DrawCircle(coord, 2);
        }
    }

    void DrawCircle(Coord c, int radius)
    {
        for (int x = -radius; x <= radius; x++)
        {
            for (int y = -radius; y <= radius; y++)
            {
                if(x*x + y*y <= radius*radius)
                {
                    int drawX = c.tileX + x;
                    int drawY = c.tileY + y;

                    if(IsInMapRange(drawX, drawY))
                    {
                        map[drawX, drawY] = 0;
                    }
                }
            }
        }
    }

    List<Coord> GetLine(Coord from, Coord to)
    {
        List<Coord> line = new List<Coord>();

        int x = from.tileX;
        int y = from.tileY;
        int dx = to.tileX - from.tileX;
        int dy = to.tileY - from.tileY;
        bool isInverted = false;
        int step = Math.Sign(dx);
        int gradientStep = Math.Sign(dy);

        int longest = Mathf.Abs(dx);
        int shortest = Mathf.Abs(dy);

        if (longest < shortest)
        {
            isInverted = true;
            longest = Mathf.Abs(dy);
            shortest  = Mathf.Abs(dx);

            step = Math.Sign(dy);
            gradientStep= Math.Sign(dx);
        }

        int gradientAccumulation = longest / 2;

        for(int i = 0; i < longest; i++)
        {
            line.Add(new Coord(x, y));

            if(isInverted)
            {
                y += step;
            }
            else
            {
                x += step;
            }

            gradientAccumulation += shortest;
            if(gradientAccumulation >= longest)
            {
                if(isInverted)
                {
                    x += gradientStep;
                }
                else
                {
                    y+= gradientStep;
                }
                gradientAccumulation -= longest;
            }
        }
        return line;
    }

    Vector3 CoordToWorldPoint(Coord tile)
    {
        return new Vector3(-width / 2 + 0.5f + tile.tileX, 2, -height / 2 + 0.5f + tile.tileY);
    }

    // Rooms are connected to the nearest room
    class Room : IComparable<Room>
    {
        public List<Coord> tiles;           // a room region
        public List<Coord> edgeTiles;
        public List<Room> connectedRooms;   // only direct connections, no transitive relation
        public int roomSize;
        public bool isAccessibleFromMainRoom;
        public bool isMainRoom;

        public Room() { }
        
        /*
         * Arrays in .NET are object on the heap, so you have a reference.
         * That reference is passed by value, meaning that changes to the contents of the array will be seen by the caller,
         * but reassigning the array won't
         */
        public Room(List<Coord> roomTiles, int[,] map)
        {
            tiles = roomTiles;
            roomSize = tiles.Count;
            connectedRooms = new List<Room>();
            edgeTiles = new List<Coord>();
            

            // calculate edgeTiles
            foreach (Coord tile in tiles)
            {
                for (int x = tile.tileX - 1; x <= tile.tileX + 1; ++x)
                {
                    for (int y = tile.tileY - 1; y <= tile.tileY + 1; ++y)
                    {
                        // exclude diagonal neighbours
                        if (x != tile.tileX && y != tile.tileY) continue;
                        
                        if (map[x, y] == (int)TileTypes.Wall)
                        {
                            edgeTiles.Add(tile);
                        }
                    }
                }
            }
        }

        public void SetAccessibleFromMainRoom()
        {
            if(!isAccessibleFromMainRoom)
            {
                isAccessibleFromMainRoom = true;
                foreach(Room connectedRoom in connectedRooms)
                {
                    connectedRoom.SetAccessibleFromMainRoom();
                }
            }
        }

        public static void ConnectRooms(Room roomA, Room roomB)
        {
            if(roomA.isAccessibleFromMainRoom)
            {
                roomB.SetAccessibleFromMainRoom();
            }
            else if(roomB.isAccessibleFromMainRoom)
            {
                roomA.SetAccessibleFromMainRoom();
            }

            roomA.connectedRooms.Add(roomB);
            roomB.connectedRooms.Add(roomA);
        }

        public bool IsConnected(Room otherRoom)
        {
            return connectedRooms.Contains(otherRoom);
        }
        
        public int CompareTo(Room other)
        {
            return other.roomSize.CompareTo(roomSize);
        }
    }
    

    private void OnDrawGizmos()
    {
        if (map == null)
            return;

        for (int x = 0; x < width; ++x)
        {
            for (int y = 0; y < height; ++y)
            {
                Gizmos.color = (map[x, y] == (int)TileTypes.Wall) ? Color.black : Color.white;
                //Gizmos.color = new Color((float)x / (width-1), (float)y / (height - 1), 0.0f);
                Vector3 pos = new Vector3(-width / 2 + x + 0.5f, 0, -height / 2 + y + 0.5f);
                Gizmos.DrawCube(pos, Vector3.one);
            }
        }
    }
}
