using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Unity.PerformanceTesting;

public class Array_Access
{
    public int width = 10000, height = 20000;
    int[,] arr;
    int[] arr_1d;

    [SetUp]
    public void Setup()
    {
        // ob width nun #rows oder #cols darstellt ist abh�ngig von der interpretation.
        // width = 5 height = 3      _ _ _ 
        //  _ _ _ _ _               |_|_|_|
        // |_|_|_|_|_|              |_|_|_|
        // |_|_|_|_|_|              |_|_|_|
        // |_|_|_|_|_|              |_|_|_|
        //                          |_|_|_|
        //
        // int[width, height] <-- Mit for x < width: for y < height: arr[x][y] l�uft man sequentiell durch. DAS IST WICHTIG DA PERFORMANCE
        // Je nachdem wie man das sp�ter visualisiert geht man "column" bzw. "row" major durch.
        //
        // Mit dem loop m�chte man nun zeilenweise f�llen, wie man es sich vorstellt. width = 5 muss somit wegen sequentiellem Zugriff die Anzahl an rows darstellen (rechtes Bild). 
        // Stellt man sich das so vor dann passt der Name 'width' aber nicht es m�sste height hei�en, da x sich ja erst nach jedem kompletten Durchlauf von y erh�ht.
        // Interpretiert man in diesem Fall x, y direkt als Position passt das ebenfalls nicht. [0][0] w�re unten links, aber [0][1] w�re nicht die 2. Spalte unten, sondern auf der y-Achse 1 h�her.
        //
        // Die Visualisierung kann nun aber anders interpretiert werden. Man nimmt f�r die xPos das y(height) und f�r die yPos das x(width).
        // Mit x = 1 und y = 2 ergibt das arr[1][2] --> xPos = y = 2, yPos = x = 1. Somit ist x nun in der Visualisierung die Zeile und das y die Spalte und entspricht dem rechten Bild.
        // Die Namen sind nat�rlich noch falsch. Also nennen wir width in dim1 und height in dim2 um, somit:
        // dim1 = 5 und dim2 = 3.
        //
        // Also: arr[dim1, dim2] 5,3 (wie zuvor)
        // Mit dem gleichen loop for x < dim1:  for y < dim2: arr[x][y] (ranges sind ok) und wenn yPos durch x und xPos durch das y repr�sentiert wird
        // w�rde dann [0][0] unten links sein, [0][1] w�re unten mitte und [0][2] unten rechts. Und y w�re komplett durchgelaufen. Passt also, wenn man das so interpretiert.
        // Es wird ZEILENWEISE gef�llt.
        // 
        //
        // Stattdessen kann man sich aber width als Anzahl Spalten und height als Anzahl Zeilen vorstellen. Also eben als width und height. Das w�re dann das linke Bild.
        // arr[x][y] w�re dann [0][0] unten links [0][1] ein dr�ber. Das w�rde mit dem Namen und mit der 1:1 �bertragung von x, y in Position �bereinstimmen.
        // In diesem Fall f�llt man SPALTENWEISE.
        //
        // Alternativ f�r zeilenweises f�llen erstellt man das array so: int[height, width];
        // for y < height: for x < width arr[y][x] <--- der Zugriff ist also anders rum, immer noch sequentiell, und man kann y als yPos und x als xPos weiterhin nutzen.
        //
        //
        arr = new int[width, height];
        arr_1d = new int[width * height];

        for (int x = 0; x < width; ++x)
        {
            for (int y = 0; y < height; ++y)
            {
                arr[x, y] = Random.Range(0, 101);
                arr_1d[y + x * width] = Random.Range(0, 101);
            }
        }
    }

    [Test, Performance]
    public void Array1D_AccessNonSeq()
    {

        Measure.Method(() =>
        {
            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < height; ++y)
                {
                    int data = arr_1d[x + y*width];
                }
            }
        })
            .MeasurementCount(10)
            .Run();
    }

    // Ergebnis: Die sequentiellen Zugriffe sind am schnellsten. 1D array ist 37.5% schneller als 2D array.
    [Test, Performance]
    public void Array1D_AccessSequential()
    {

        Measure.Method(() =>
        {
            for (int y = 0; y < height; ++y)
            {
                for (int x = 0; x < width; ++x)
                {
                    int data = arr_1d[x + y*width];
                }
            }
        })
            .MeasurementCount(10)
            .Run();
    }

    [Test, Performance]
    public void Array2D_AccessSequential()
    {

        Measure.Method(() =>
        {
            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < height; ++y)
                {
                    int data = arr[x, y];
                }
            }
        }).MeasurementCount(10)
          .Run();
    }


    [Test, Performance]
    public void Array2D_AccessNonSeq()
    {

        Measure.Method(() =>
        {
            for (int y = 0; y < height; ++y)
            {
                for (int x = 0; x < width; ++x)
                {
                    int data = arr[x, y];
                }
            }
        }).MeasurementCount(10)
          .Run();
    }


}
